# Priorities Automation

This project auto generates updates to a groups priorities YAML file. There are four main functions in this repo, two each for planning and weekly updates (push and pull). The repo has been created to be run through GitLab scheduled pipelines.

## Functions

### `create_weekly_update`

This functions will auto generate an MR to update the priorities YAML `progress` field. This creates the ability for the group EM and PM to review and update the fields as necessary, specifically focusing on `status`, `progress` and `target_release`.

It loops through all of a groups Epics that have the ~direction label attached.

- It will add any new Epics to the YAML with default parameters.
- If the Epics is in the YAML it will update the `name` field and resets the `progress` field to empty.
- If there is an entry in the YAML but not the Epics, it will be moved to the closed section

### `get_weekly_update`

This functions will generate an output of the current in progress priorities and where they are currently at. It gets all priorities that have a `progress` field filled in. Its generates a GitLab markdown table output.

The fields generated in the output:

- `url`
- `status`
- `progress`
- `milestone_goal`
- `target_release`

### `create_planning_update`

This functions will auto generate an MR to update the priorities YAML `milestone_goal` field. This creates the ability for the group EM and PM to review and update the fields as necessary, specifically focusing on `status`, `milestone_goal` and `target_release`.

It loops through all of a groups Epics that have the ~direction label attached.

- It will add any new Epics to the YAML with default parameters.
- If the Epics is in the YAML it will update the `name` field and resets the `progress` field to empty.
- If there is an entry in the YAML but not the Epics, it will be moved to the closed section

### `get_planning_update`

This functions will generate an output of the current in progress priorities so that they can be discussed for milestone planning. It gets all priorities that have a `progress` field filled in. Its generates a GitLab markdown table output.

The fields generated in the output:

- `url`
- `dri`
- `progress`
- `milestone_goal`
- `target_release`

## Setup

1. Clone this repo.
2. Copy the environment variables from `.env` and create variables in the [GitLab CI/CD variables](https://docs.gitlab.com/ee/ci/variables/index.html)
3. Create 3 new [GitLab Pipeline Schedules](https://docs.gitlab.com/ee/ci/pipelines/schedules.html)
4. Each schedule should use the function name above as Description
5. I recommend the following schedules:
6. `create_weekly_update` - Weekly on Thurs (`30 10 * * 4`)
6. `create_planning_update` - Monthly on last Mon (`30 10 10 * *`)
7. `get_weekly_update` - Yearly, you will run this manually once the above is merged (`30 10 1 1 *`)
8. `get_planning_update` - Yearly, you will run this manually when starting planning ~15th (`30 10 1 1 *`)
9. Create a Variables for each scheduled function called `SCHEDULE_TYPE` ensure that this value is set to the function name eg `create_weekly_update`, `get_weekly_update`, `create_planning_update` or `get_planning_update`

## Environment variables

- `GITLAB_ACCESS_KEY` A personal access token to connect to GitLab API https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
- `EPIC_PROJECT_ID` the project that you want to search for open Epics for the group priorities (GitLab.com: 9970)
- `GROUP` the group name that you want to use for branch names, must be url safe eg `compliance_group`
- `GROUP_LABEL` the group label that you want to use to get the epics
- `GROUP_YAML` The URL of the YAML file to update, see YAMl below
- `GROUP_YAML_LOCAL` The local path of the YAML file in its project
- `GROUP_PRIORITIES` A public link to the display priorities, this is for link purposes
- `GROUP_EM_ID` The groups EM GitLab ID
- `GROUP_EM` The groups EM GitLab handle
- `GROUP_PM_ID` The groups PM GitLab ID
- `GROUP_PM` The groups PM GitLab handle
- `YAML_PROJECT_ID` The project ID of the project where the YAML file stored (GitLab handbook: 7764)
- `YAML_PROJECT_REF_BRANCH` The reference branch to use for creating MR in project identified above
- `ENABLE_LOGGING` Whether to enable logging, use it locally ;)
- `ISSUE_PROJECT_ID` The project ID where the weekly update and planning issues will be
- `WEEKLY_UPDATE_ISSUE` The issue ID where the weekly update comment will be posted
- `NEXT_MILESTONE_ISSUE` The issue ID where the planning update comment will be posted

## YAML File

The YAML file that this automation updates needs to have the following structure:

```
priorities:
- name: ""              // Automatically updated field
  url: ""               // Automatically updated field
  dri: ""               // Manual field
  target_release: ""    // Manual field
  status: ""            // Manual field
  progress: ""          // Manual field (create_weekly_update)
  milestone_goal: ""    // Manual field (create_planning_update)
  priority: ""          // Automatically updated field

closed:
- name: ""
  url: ""
  dri: ""
  target_release: ""
  status: ""
  progress: ""
  milestone_goal: ""
  priority: ""
```

## Running locally

This repo uses node/express and can be run locally

Clone down the repo

Run `npm install` to install all dependencies

Next you will need to update the config and add an access token

1. Copy `.env.example` over to `.env`
1. Update the variable `GITLAB_ACCESS_KEY` with a personal access token. How to get one https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
1. Update the other variables as required

Run `npm run start` to start the project

Open your browser to http://localhost:3001/<FUNCTION>. This will run the function and create the output.
