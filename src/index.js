import express, { json } from 'express';
import * as dotenv from 'dotenv'
dotenv.config();
import { createWeeklyUpdate } from './funcs/create_weekly_update.js';
import { createPlanningUpdate } from './funcs/create_planning_update.js';
import { getWeeklyUpdate } from './funcs/get_weekly_update.js';
import { getPlanningUpdate } from './funcs/get_planning_update.js';

const app = express();
const PORT = process.env.PORT || 3001;

app.get("/create_weekly_update", async (request, response, next) => {

    try {

        const createWeeklyUpdateResponse = await createWeeklyUpdate();

        response.json(createWeeklyUpdateResponse);
    } catch(error) {
        console.log({error})
        next(error);
    }
});

app.get("/create_planning_update", async (request, response, next) => {

    try {

        const createPlanningUpdateResponse = await createPlanningUpdate();

        response.json(createPlanningUpdateResponse);
    } catch(error) {
        console.log({error})
        next(error);
    }
});

app.get("/get_weekly_update", async (request, response, next) => {

    try {

        const getWeeklyUpdateResponse = await getWeeklyUpdate();

        response.send(getWeeklyUpdateResponse);
    } catch(error) {
        console.log({error})
        next(error);
    }
});

app.get("/get_planning_update", async (request, response, next) => {

    try {

        const getPlanningUpdateResponse = await getPlanningUpdate();

        response.send(getPlanningUpdateResponse);
    } catch(error) {
        console.log({error})
        next(error);
    }
});

app.use((error, request, response, next) => {
    response.status(500).send(`Error: ${error}`);
})

app.listen(PORT, () => console.log(`App listening at port ${PORT}`));
