import yaml from "js-yaml";

export const convertYamlToJson = async ({ yml = null }) => {
    try {
        const json = yaml.dump(yml, { quotingType: '"', forceQuotes: true });

        return json;
    } catch (err) {
        const error = err.message;

        throw new Error(error);
    }
};
