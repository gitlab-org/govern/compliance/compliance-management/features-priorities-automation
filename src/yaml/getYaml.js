import fetch from 'node-fetch';
import yaml from "js-yaml";

export const getYaml = async ({ url = null }) => {
    try {
        const response = await fetch(url);
        const body = await response.text();
        const doc = yaml.load(body);

        return doc;
    } catch (err) {
        const error = err.message;

        throw new Error(error);
    }
};
