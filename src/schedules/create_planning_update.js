import { createPlanningUpdate } from '../funcs/create_planning_update.js';

try {
    const createPlanningUpdateResponse = await createPlanningUpdate();
    console.log({ createPlanningUpdateResponse });

} catch(error) {
    console.log({ error });
}