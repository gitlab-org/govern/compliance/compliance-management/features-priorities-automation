import { createWeeklyUpdate } from '../funcs/create_weekly_update.js';

try {
    const createWeeklyUpdateResponse = await createWeeklyUpdate();
    console.log({ createWeeklyUpdateResponse });

} catch(error) {
    console.log({ error });
}