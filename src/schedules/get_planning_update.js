import { getPlanningUpdate } from '../funcs/get_planning_update.js';

try {
    const getPlanningUpdateResponse = await getPlanningUpdate({ new_line: '\n' });
    console.log({ getPlanningUpdateResponse });

} catch(error) {
    console.log({ error });
}