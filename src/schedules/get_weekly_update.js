import { getWeeklyUpdate } from '../funcs/get_weekly_update.js';

try {
    const getWeeklyUpdateResponse = await getWeeklyUpdate({ new_line: '\n' });
    console.log({ getWeeklyUpdateResponse });

} catch(error) {
    console.log({ error });
}