import https from 'https';

import { 
    getDRI,
    getState,
    getPriority,
    getDivision,
    getRoadmap,
    getTargetRelease,
    getStatus,
    getProgress,
} from '../utils/index.js';

export const getEpicLatest = async ({ project_id = null, epic = {} }) => {

    if(epic) {
        const progress_amount = await getProgress({ project_id, epic });

        return {
            name: epic.title,
            url: epic.web_url,
            state: getState({ epic }),
            //dri: getDRI({ epic }),
            priority: getPriority({ epic }),
            division: getDivision({ epic }),
            roadmap: getRoadmap({ epic }),
            target_release: getTargetRelease({ epic }),
            status: getStatus({ epic, progress_amount }),
            progress_amount,
        }
    }
};