import https from 'https';

export const enableRequestLogger = () => {
    const original = https.request
    https.request = function(options, callback){
        return original(options, callback);
    }
};