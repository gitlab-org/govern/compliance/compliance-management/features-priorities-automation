export const inArrayOfObjects = ({array = [], propertyName = null, searchString = null} = {}) => {
    return array.some(item => item[propertyName] === searchString);
};