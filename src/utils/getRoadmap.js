import https from 'https';

export const getRoadmap = ({ epic = {} }) => {

    const roadmap = epic.labels.find((str) => str.includes("roadmap:"));

    return roadmap;
};