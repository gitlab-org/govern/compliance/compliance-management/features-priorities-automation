import https from 'https';

export const getStatus = ({ epic = {}, progress_amount = null }) => {

    const currentDate = new Date();
    const endDate = new Date(epic.due_date_from_milestones);
    
    if (isNaN(endDate.getTime())) {
        throw new Error("Invalid projected end date");
    }
    
    const totalDuration = endDate - currentDate;
    const elapsedDuration = totalDuration > 0 ? ((currentDate - new Date()) / totalDuration) * 100 : 100;
    
    if (progress_amount === 0.00) {
        return "NOT STARTED";
    } else if (progress_amount >= elapsedDuration) {
        return "ON TRACK";
    } else if (progress_amount >= elapsedDuration - 20) {
        return "NEEDS ATTENTION";
    } else {
        return "AT RISK";
    }
};