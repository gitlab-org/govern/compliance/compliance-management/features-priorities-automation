import https from 'https';

import { getEpicsIssue } from "../gitlab/index.js";

export const getProgress = async ({ project_id = null, epic = {} }) => {

    const issues = await getEpicsIssue({ project_id, epic_id: epic.iid });

    const totalIssues = issues.length;
    const closedIssues = issues.filter(issue => issue.state === "closed").length;
    const progress = totalIssues > 0 ? (closedIssues / totalIssues) * 100 : 0;

    return progress.toFixed(2);
};