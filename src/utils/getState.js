import https from 'https';

export const getState = ({ epic = {} }) => {

    const state = epic.labels.find((str) => str.includes("status:"));

    return state ? state : 'status::uncategorised'
};