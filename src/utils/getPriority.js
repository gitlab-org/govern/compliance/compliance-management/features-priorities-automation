import https from 'https';

export const getPriority = ({ epic = {} }) => {

    const priority = epic.labels.find((str) => str.includes("priority:"));

    return priority
};