export const findInArrayOfObjects = ({array = [], propertyName = null, searchString = null} = {}) => {
    return array.find(item => item[propertyName] === searchString) || null;
};