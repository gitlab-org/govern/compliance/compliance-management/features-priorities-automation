import https from 'https';

export const getTargetRelease = ({ epic = {} }) => {

    const roadmap = epic.labels.find((str) => str.includes("roadmap:"));

    return roadmap.substring(9);
};