import { gitlabApi } from "../hooks/index.js";

export const createNewBranch = async ({ project_id = null, name = null, ref_branch = null }) => {
    try {
        // https://docs.gitlab.com/ee/api/branches.html#create-repository-branch
        const branch = await gitlabApi.Branches.create(
            project_id,
            name,
            ref_branch,
        );

        return branch;
    } catch (err) {
        const error = `createNewBranch: ${err.message}`;

        throw new Error(error);
    }
};
