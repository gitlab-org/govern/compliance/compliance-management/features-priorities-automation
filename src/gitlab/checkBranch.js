import { gitlabApi } from "../hooks/index.js";

export const checkBranch = async ({ project_id = null, name = null }) => {
    try {
        // https://docs.gitlab.com/ee/api/branches.html#get-single-repository-branch
        const branch = await gitlabApi.Branches.show(
            project_id,
            name,
        );

        return branch;
    } catch (err) {
        const error = `checkBranch: ${err.message}`;

        return false;

        //throw new Error(error);
    }
};
