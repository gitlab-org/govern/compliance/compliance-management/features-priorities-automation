import { gitlabApi } from "../hooks/index.js";

export const getEpics = async ({ project_id = null, group_label = null }) => {
    try {
        const queryParams = { 
            labels: `${group_label}, direction`,
            per_page: `100`,
            state: 'opened'
        };

        // https://gitlab.com/api/v4/groups/9970/epics?labels=group::compliance,direction&per_page=100
        // https://docs.gitlab.com/ee/api/epics.html
        const epics = await gitlabApi.Epics.all(project_id, queryParams);

        return epics;
    } catch (err) {
        const error = `getEpics: ${err.message}`;

        throw new Error(error);
    }
};
