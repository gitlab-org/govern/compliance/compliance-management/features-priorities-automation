import { gitlabApi } from "../hooks/index.js";

export const pushNewCommit = async ({
    project_id = null,
    branch = null,
    file_path = null,
    content = null,
    commit_message = null
}) => {
    try {
        const actions = [
            {
                action: "update",
                file_path,
                content
            }
        ]

        // https://docs.gitlab.com/ee/api/commits.html
        const commit = await gitlabApi.Commits.create(
            project_id,
            branch,
            commit_message,
            actions
        );

        return commit;
    } catch (err) {
        const error = `pushNewCommit: ${err.message}`;

        throw new Error(error);
    }
};
