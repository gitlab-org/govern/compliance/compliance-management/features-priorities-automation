import { gitlabApi } from "../hooks/index.js";

export const createMR = async ({
    title = null,
    project_id = null,
    source_branch = null,
    target_branch = null,
    assignee_ids = null,
    remove_source_branch = null,
    description = null,
} = {}) => {
    try {
        const options = {
            assignee_ids,
            remove_source_branch,
            description
        };

        // https://docs.gitlab.com/ee/api/merge_requests.html#create-mr
        const merge_request = await gitlabApi.MergeRequests.create(
            project_id,
            source_branch,
            target_branch,
            title,
            options
        );

        return merge_request;
    } catch (err) {
        const error = `createMR: ${err.message}`;

        throw new Error(error);
    }
};
