import { gitlabApi } from "../hooks/index.js";

export const getEpicsIssue = async ({ project_id = null, epic_id = null }) => {
    try {
        // https://gitlab.com/api/v4/groups/9970/epics/16499/issues
        // https://docs.gitlab.com/api/epic_issues/
        const issues = await gitlabApi.EpicIssues.all(project_id, epic_id);

        return issues;
    } catch (err) {
        const error = `getEpicIssue: ${err.message}`;

        throw new Error(error);
    }
};
