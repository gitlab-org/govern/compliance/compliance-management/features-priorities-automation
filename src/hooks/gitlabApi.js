// https://github.com/jdalrymple/gitbeaker
import { Gitlab } from '@gitbeaker/rest';
import * as dotenv from 'dotenv'
dotenv.config();

const token = process.env.GITLAB_ACCESS_KEY;
const host = 'https://gitlab.com/';

// setup Gitlab API and export for use
export const gitlabApi = new Gitlab({
    host,
    token,
});
