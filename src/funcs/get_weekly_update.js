import * as dotenv from 'dotenv'
dotenv.config();
import { enableRequestLogger } from '../utils/index.js';
import { getYaml } from '../yaml/index.js';
import { createComment } from "../gitlab/index.js";

if (process.env.ENABLE_LOGGING){
  enableRequestLogger();
}

export const getWeeklyUpdate = async ({ new_line = '\n' } = {}) => {

    const report = [];

    try {
        report.push(`### Weekly [Priorities](${process.env.GROUP_PRIORITIES}) Update`);
        report.push(new_line);
        report.push(` | Priority | Division | Epic | Status | Progress | Update | Milestone Goal | Target Release | `);
        report.push(`| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | `);

        //Get Priorities
        const yaml = await getYaml({ url: process.env.GROUP_YAML });
        const priorities = [...yaml.now];

        // Loop through priorities
        for (const priority of priorities) {
            report.push(` | ${priority?.priority.substring(10)} | ${priority?.division.substring(10)} | ${priority?.url}+ | **${priority?.status}** | **${priority?.progress_amount}**% | ${priority?.progress} | ${priority?.milestone_goal} | ${priority?.target_release} | `);
        };

        const comment_url = await createComment({ 
            project_id: process.env.ISSUE_PROJECT_ID,
            issue_id: process.env.WEEKLY_UPDATE_ISSUE,
            body: report.join(new_line)
        });

        return comment_url;
    } catch(error) {
        throw new Error(error);
    }
};
