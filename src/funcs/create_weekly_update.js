import * as dotenv from 'dotenv'
dotenv.config();
import { getCurrentDate } from '../utils/index.js';
import { 
    enableRequestLogger,
    findInArrayOfObjects,
    inArrayOfObjects,
    getEpicLatest
} from '../utils/index.js';
import { getYaml, convertYamlToJson } from '../yaml/index.js';
import { 
    getEpics,
    checkBranch,
    createNewBranch,
    pushNewCommit,
    createMR
} from "../gitlab/index.js";

if (process.env.ENABLE_LOGGING){
  enableRequestLogger();
}

const priorityTemplate = {
    name: "",
    url: "",
    dri: "",
    priority: "",
    division: "",
    roadmap: "",
    target_release: "",
    status: "NOT STARTED",
    progress_amount: "",
    milestone_goal: "",
    progress: ""
}

export const createWeeklyUpdate = async () => {

    const returnObject = {};
    const nowPrioritiesList = [];
    const nextPrioritiesList = [];
    const laterPrioritiesList = [];
    const ongoingPrioritiesList = [];
    const uncategorisedPrioritiesList = [];

    try {
        //Get Epics
        const epics = await getEpics({ 
            project_id: process.env.EPIC_PROJECT_ID,
            group_label: process.env.GROUP_LABEL
        });

        //Get Priorities
        const yaml = await getYaml({ url: process.env.GROUP_YAML });
        const nowPriorities = [...yaml.now];
        const nextPriorities = [...yaml.next];
        const laterPriorities = [...yaml.later];
        const ongoingPriorities = [...yaml.ongoing];
        const uncategorisedPriorities = [...yaml.uncategorised];
        const closedPriorities = [...yaml.closed];

        const priorities = [...nowPriorities, ...nextPriorities, ...laterPriorities, ...ongoingPriorities, ...uncategorisedPriorities];

        // Loop through priorities
        for (const priority of priorities) {
            const epic = findInArrayOfObjects({array: epics, propertyName: 'web_url', searchString: priority.url})

            //Get latest info
            const latest_epic = await getEpicLatest({ project_id: process.env.EPIC_PROJECT_ID, epic });

            if(epic){
                if(latest_epic.state === "status::now"){
                    //Reset milestone goal
                    nowPrioritiesList.push({
                        ...priority,
                        ...latest_epic,
                        progress: ""
                    });
                } else if(latest_epic.state === "status::next"){
                    nextPrioritiesList.push({
                        ...priority,
                        ...latest_epic,
                        status: "NOT STARTED",
                        milestone_goal: "",
                        progress: ""
                    });
                } else if(latest_epic.state === "status::later"){
                    laterPrioritiesList.push({
                        ...priority,
                        ...latest_epic,
                        status: "NOT STARTED",
                        milestone_goal: "",
                        progress: ""
                    });
                } else if(latest_epic.state === "status::ongoing"){
                    ongoingPrioritiesList.push({
                        ...priority,
                        ...latest_epic,
                        status: "NOT STARTED",
                        milestone_goal: "",
                        progress: ""
                    });
                } else if(latest_epic.state === "status::uncategorised"){
                    uncategorisedPrioritiesList.push({
                        ...priority,
                        ...latest_epic,
                        status: "NOT STARTED",
                        milestone_goal: "",
                        progress: ""
                    });
                }
            } else {
                // Close priority
                closedPriorities.push({
                    ...priority,
                    ...latest_epic,
                    status: "CLOSED",
                    progress: "",
                    milestone_goal: ""
                });
            }

        };
    
        // Loop through remaining epics and add to bottom of final list
        for (const epic of epics) {
            if( !inArrayOfObjects({array: priorities, propertyName: 'url', searchString: epic.web_url}) ){   
                
                //Get latest info
                const latest_epic = await getEpicLatest({ project_id: process.env.EPIC_PROJECT_ID, epic });

                if(latest_epic.state === "status::now"){
                    //Reset milestone goal
                    nowPrioritiesListList.push({
                        ...priorityTemplate,
                        ...latest_epic,
                    });
                } else if(latest_epic.state === "status::next"){
                    nextPrioritiesList.push({
                        ...priorityTemplate,
                        ...latest_epic,
                    });
                } else if(latest_epic.state === "status::later"){
                    laterPrioritiesList.push({
                        ...priorityTemplate,
                        ...latest_epic,
                    });
                } else if(latest_epic.state === "status::ongoing"){
                    ongoingPrioritiesList.push({
                        ...priorityTemplate,
                        ...latest_epic,
                    });
                } else if(latest_epic.state === "status::uncategorised"){
                    uncategorisedPrioritiesList.push({
                        ...priorityTemplate,
                        ...latest_epic,
                    });
                }              
            }
        };

        // Order arrays by priority
        nowPrioritiesList.sort((a, b) => {
            if (a.priority > b.priority)
                return 1;
            if (a.priority < b.priority)
                return -1;
            return 0;
        });
        nextPrioritiesList.sort((a, b) => {
            if (a.priority > b.priority)
                return 1;
            if (a.priority < b.priority)
                return -1;
            return 0;
        });
        laterPrioritiesList.sort((a, b) => {
            if (a.priority > b.priority)
                return 1;
            if (a.priority < b.priority)
                return -1;
            return 0;
        });
        ongoingPrioritiesList.sort((a, b) => {
            if (a.priority > b.priority)
                return 1;
            if (a.priority < b.priority)
                return -1;
            return 0;
        });
        uncategorisedPrioritiesList.sort((a, b) => {
            if (a.priority > b.priority)
                return 1;
            if (a.priority < b.priority)
                return -1;
            return 0;
        });
                
        const name = `${process.env.GROUP}_${process.env.GROUP_EM}_create_weekly_update_${getCurrentDate()}`;

        const branchExists = await checkBranch({
            project_id: process.env.YAML_PROJECT_ID,
            name,
        });

        if(!branchExists){
            const branch = await createNewBranch({
                project_id: process.env.YAML_PROJECT_ID,
                name,
                ref_branch: process.env.YAML_PROJECT_REF_BRANCH,
            });

            const nowPrioritiesJson = await convertYamlToJson({ yml: nowPrioritiesList });
            const nextPrioritiesJson = await convertYamlToJson({ yml: nextPrioritiesList });
            const laterPrioritiesJson = await convertYamlToJson({ yml: laterPrioritiesList });
            const ongoingPrioritiesJson = await convertYamlToJson({ yml: ongoingPrioritiesList });
            const uncategorisedPrioritiesJson = await convertYamlToJson({ yml: uncategorisedPrioritiesList });
            const closedPrioritiesJson = await convertYamlToJson({ yml: closedPriorities });

            const commit = await pushNewCommit({ 
                project_id: process.env.YAML_PROJECT_ID,
                branch: name,
                file_path: process.env.GROUP_YAML_LOCAL,
                content: `now:\n${nowPrioritiesJson}next:\n${nextPrioritiesJson}later:\n${laterPrioritiesJson}ongoing:\n${ongoingPrioritiesJson}uncategorised:\n${uncategorisedPrioritiesJson}\nclosed:\n${closedPrioritiesJson}`,
                commit_message: `${process.env.GROUP_LABEL} Weekly priorities update ${getCurrentDate()}`
            });
            
            const merge_request = await createMR({ 
                project_id: process.env.YAML_PROJECT_ID,
                source_branch: name,
                target_branch: process.env.YAML_PROJECT_REF_BRANCH,
                title: `${process.env.GROUP_LABEL} Weekly priorities update ${getCurrentDate()}`,
                description: `Update the 'progress' of 'status:now' priorities`,
                assignee_ids: [
                    process.env.GROUP_EM_ID,
                    process.env.GROUP_PM_ID,
                ],
                labels: process.env.GROUP_LABEL,
                remove_source_branch: true
            });

            returnObject.web_url = merge_request.web_url
        } else {
            returnObject.web_url = branchExists.web_url
        }

        return returnObject;
    } catch(error) {
        throw new Error(error);
    }
};
