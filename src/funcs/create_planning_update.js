import * as dotenv from 'dotenv'
dotenv.config();
import { getCurrentDate } from '../utils/index.js';
import { enableRequestLogger, findInArrayOfObjects } from '../utils/index.js';
import { getYaml, convertYamlToJson } from '../yaml/index.js';
import { 
    checkBranch,
    createNewBranch,
    pushNewCommit,
    createMR
} from "../gitlab/index.js";

if (process.env.ENABLE_LOGGING){
  enableRequestLogger();
}

export const createPlanningUpdate = async () => {

    const returnObject = {};
    const newPriorities = [];

    try {
        //Get Priorities
        const yaml = await getYaml({ url: process.env.GROUP_YAML });
        const priorities = [...yaml.priorities];
        const closedPriorities = [...yaml.closed];

        // Loop through priorities
        for (const priority of priorities) {
            if(priority.milestone_goal !== ""){
                //Reset milestone goal
                newPriorities.push({
                    ...priority,
                    milestone_goal: "",
                });
            } else {
                // Close priority
                newPriorities.push(priority);
            }

        };

        const name = `${process.env.GROUP}_${process.env.GROUP_EM}_planning_update_${getCurrentDate()}`;

        const branchExists = await checkBranch({
            project_id: process.env.YAML_PROJECT_ID,
            name,
        });

        if(!branchExists){
            const branch = await createNewBranch({
                project_id: process.env.YAML_PROJECT_ID,
                name,
                ref_branch: process.env.YAML_PROJECT_REF_BRANCH,
            });

            const prioritiesJson = await convertYamlToJson({ yml: newPriorities });
            const closedPrioritiesJson = await convertYamlToJson({ yml: closedPriorities });
            const commit = await pushNewCommit({ 
                project_id: process.env.YAML_PROJECT_ID,
                branch: name,
                file_path: process.env.GROUP_YAML_LOCAL,
                content: `priorities:\n${prioritiesJson}\nclosed:\n${closedPrioritiesJson}`,
                commit_message: `${process.env.GROUP_LABEL} Planning update ${getCurrentDate()}`
            });
            
            const merge_request = await createMR({ 
                project_id: process.env.YAML_PROJECT_ID,
                source_branch: name,
                target_branch: process.env.YAML_PROJECT_REF_BRANCH,
                title: `${process.env.GROUP_LABEL} Planning update ${getCurrentDate()}`,
                description: `Update the 'status' and 'milestone_goal' of in progress priorities`,
                assignee_ids: [
                    process.env.GROUP_EM_ID,
                    process.env.GROUP_PM_ID,
                ],
                labels: process.env.GROUP_LABEL,
                remove_source_branch: true
            });

            returnObject.web_url = merge_request.web_url
        } else {
            returnObject.web_url = branchExists.web_url
        }

        return returnObject;
    } catch(error) {
        throw new Error(error);
    }
};
