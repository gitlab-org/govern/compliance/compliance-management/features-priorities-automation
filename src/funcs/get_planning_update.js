import * as dotenv from 'dotenv'
dotenv.config();
import { enableRequestLogger } from '../utils/index.js';
import { getYaml } from '../yaml/index.js';
import { createComment } from "../gitlab/index.js";

if (process.env.ENABLE_LOGGING){
  enableRequestLogger();
}

export const getPlanningUpdate = async ({ new_line = '\n' } = {}) => {

    const report = [];

    try {

        report.push(`### Planning [Priorities](${process.env.GROUP_PRIORITIES})`);
        report.push(new_line);
        report.push(`| Priority | DRI | Milestone Goal | Target Release | `);
        report.push(`| ------ | ------ | ------ | ------ | `);

        //Get Priorities
        const yaml = await getYaml({ url: process.env.GROUP_YAML });
        const priorities = [...yaml.priorities];

        // Loop through priorities
        for (const priority of priorities) {
            if(priority.milestone_goal !== ""){
                //Reset milestone goal
                report.push(`| ${priority?.url}+ | \`@${priority?.dri}\`  | ${priority?.milestone_goal} | ${priority?.target_release} | `);
            }

        };

        const comment_url = await createComment({ 
            project_id: process.env.ISSUE_PROJECT_ID,
            issue_id: process.env.NEXT_MILESTONE_ISSUE,
            body: report.join(new_line)
        });

        return comment_url;
    } catch(error) {
        throw new Error(error);
    }
};
